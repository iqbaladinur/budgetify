import Vue from 'vue'
import VueRouter from 'vue-router'
const Home = () =>  import(/* webpackChunkName: "Home" */ "@/components/pages/home/index.vue")
const MyWallets = () =>  import(/* webpackChunkName: "MyWallets" */ "@/components/pages/my-wallet/index.vue")
Vue.use(VueRouter)
const router =  new VueRouter({
    mode:"history",
    routes: [
        {
            path:'/',
            name:'home',
            component: Home
        },
        {
            path:'/my-wallet.html',
            name:'mywallets',
            component: MyWallets
        }
    ]
})

export default router