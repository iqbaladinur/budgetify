export default {
    currencyFormat: (countryCode, currency, nomina) => {
        return new Intl.NumberFormat(countryCode, {
            style: "currency",
            currency: currency
        }).format(nomina);
    }
}