import wallet from "@/db/wallet";
export default {
    addWallet: function (walletName, totalBalance, remainingBalance, color) {
        const dateString = new Date().toISOString();
        const walletDoc = {
            "_id" : walletName.replace(/\s+/g, '').toLowerCase(),
            "wallet_name": walletName,
            "total_balance": totalBalance,
            "remaining_balance": remainingBalance,
            "color":color,
            "created_at": dateString,
            "updated_at": dateString
        }
        return new Promise((resolve, reject) => {
            wallet.put(walletDoc)
                .then(() => {
                    return wallet.get(walletDoc._id);
                })
                .then((result) => {
                    resolve(result);
                })
                .catch((e) => {
                    reject(e);
                });
        });
    },
    getAllWallet: function(){
        return new Promise((resolve, reject) => {
            wallet.allDocs({include_docs: true})
                .then((result) => {
                    resolve(result);
                })
                .catch((error) => {
                    reject(error);
                });
        })
    },
    getSingleWallet: function(_id){
        return new Promise((resolve, reject) => {
            wallet.get(_id)
                .then(doc => resolve(doc))
                .catch(e => reject(e));
        });
    },
    removeSingleWallet: function(doc){
        return new Promise((resolve, reject) => {
            wallet.remove(doc)
                .then(result => resolve(result))
                .catch(e => reject(e));
        });
    }
}